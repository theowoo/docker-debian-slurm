FROM debian:stretch-slim

ENV MUNGE_VER="0.5.13"
ENV SLURM_VER="17-11-13-2"
ENV OPENMPI_VER="4.0.1"
ENV OPENMPI_VER_ABREV="v4.0"

RUN apt-get update && \
    apt-get -y install gcc g++ gfortran libgcrypt20-dev libncurses5-dev make python wget && \
    rm -rf /var/lib/apt/lists/* && \
    \
    # Optain Munge, Slurm and OpenMPI \
    wget -O /root/munge-${MUNGE_VER}.tar.xz https://github.com/dun/munge/releases/download/munge-${MUNGE_VER}/munge-${MUNGE_VER}.tar.xz && \
    wget -O /root/slurm-${SLURM_VER}.tar.gz https://github.com/SchedMD/slurm/archive/slurm-${SLURM_VER}.tar.gz && \
    wget -O /root/openmpi-${OPENMPI_VER}.tar.bz2 https://www.open-mpi.org/software/ompi/${OPENMPI_VER_ABREV}/downloads/openmpi-${OPENMPI_VER}.tar.bz2 && \
    \
    # Install Munge, Slurm and OpenMPI, and remove source \
    mkdir -p /root/local/src && \
    \
    cd /root/local/src && tar axvf /root/munge-${MUNGE_VER}.tar.xz && cd /root/local/src/munge-${MUNGE_VER} && \
    ./configure --prefix=/usr/local && \
    make -j && make install && \
    rm -rf /root/local/src/munge-${MUNGE_VER} /root/munge-${MUNGE_VER}.tar.xz && \
    \
    cd /root/local/src && tar axvf /root/slurm-${SLURM_VER}.tar.gz && cd /root/local/src/slurm-slurm-${SLURM_VER} && \
    ./configure --prefix=/usr/local && \
    make -j && make install && \
    rm -rf /root/local/src/slurm-slurm-${SLURM_VER} /root/slurm-${SLURM_VER}.tar.gz && \
    \
    cd /root/local/src && tar axvf /root/openmpi-${OPENMPI_VER}.tar.bz2 && cd /root/local/src/openmpi-${OPENMPI_VER} && \
    ./configure --prefix=/usr/local --with-pmi=/usr/local && \
    make -j && make install && \
    rm -rf /root/local/src/openmpi-${OPENMPI_VER} /root/openmpi-${OPENMPI_VER}.tar.bz2 && \
    echo 'btl_tcp_if_exclude = lo,docker0' >> /usr/local/etc/openmpi-mca-params.conf && \
    cp /usr/local/lib/libmpi_usempif08.so.40 /usr/lib/libmpi_usempi.so.40 && ldconfig && \
    \
    useradd munge -m && \
    useradd slurm -m && \
    mkdir /tmp/slurm && \
    chown slurm:slurm -R /tmp/slurm && \
    \
    apt-get remove --purge --autoremove -y python gcc g++ gfortran make libgcrypt20-dev libncurses5-dev && \
    apt-get clean

COPY scripts/munged.sh scripts/slurm-config.sh scripts/leader.sh /scripts/
COPY config/slurm.conf.template /usr/local/etc/